resource "aws_s3_bucket" "example" {
  bucket = "clause-ai-s3-buck-97865"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}