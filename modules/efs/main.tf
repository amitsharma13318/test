resource "aws_efs_file_system" "foo" {
  creation_token = "my-product-9078"

  tags = {
    Name = "MyProduct"
  }
}